import {LitElement, html,css} from 'lit-element';
import GetMyPostsDm from './get-my-posts-dm.js';

class MyElement extends LitElement {
  static get styles(){
    return css`
      #title{
        font-size: 40px;
        font-family : 'Arial';
        color: #3733FF;
        text-align: center
      }
      #number, p{
        font-size: 20px;
        font-family: 'Arial';
        text-align: center;
        display: block;
      }
      ul{
        display: flex; 
        justify-content: center;
        alig-item: center;
        font-family: 'Arial';
        font-size : 20px;
        flex-direction: column;
      }
      li{
        margin: auto;
      }
      .ctn-btn{
        text-align: center;
      }
      #btn{
        background-color : black;
        color: white;
        font-family: 'Arial';
        font-size: 20px;
        padding: 5px 30px 5px 30px;
        border-radius: 8px;
        box-shadow: 1px 1px 10px #3733FF;
        cursor: pointer;
      }
    `;
  }
  static get properties(){
    return{
      propObj :{type: Object},
      propStr : {type: String},
      users : {type: Array},
      propNum : {type: Number},

    }
  }
  constructor(){
   super();
   this.propObj = {};
   this.propStr = '';
   this.users = [''];
   this.propNum = 0;

  }
  _getPosts(){
    let _dmGetPosts = new GetMyPostsDm();
    _dmGetPosts.addEventListener('success-call', this._setPosts.bind(this));
    _dmGetPosts.addEventListener('error-call', this._showModalError.bind(this));
    _dmGetPosts.generateRequest();
  }

  _showModalError(configError){
    this.errorModal = configError;
  }
  _setPosts(data){
    this.users = data.detail;
  }
  render() {
    return html`
      <div id="title">${this.propStr}</div>
      <label id="number">${this.propNum}</label>
      <ul>
            ${this.users.map(item =>
              html`<li>${item.name}   ${item.age}</li>`
              )}
      </ul>
      <label>${this.propObj.name}</label>
      <p>Click for generate names</p>
      <div class="ctn-btn"><button id="btn" @click="${this._getPosts}">Click</button></div>
    `;
  }

}

customElements.define('my-element', MyElement);




